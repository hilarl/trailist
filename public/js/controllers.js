'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('AppCtrl', function ($scope, $http) {

    $http({
      method: 'GET',
      url: '/api/name'
    }).
    success(function (data, status, headers, config) {
      $scope.name = data.name;
    }).
    error(function (data, status, headers, config) {
      $scope.name = 'Error!';
    });

  }).
  controller('IndexController', function ($scope) {

    eval(
      function(p,a,c,k,e,d) {
        e=function(c) {
          return c
        };
        if (!''.replace(/^/,String) ){while(c--) {
          d[c]=k[c]||c}k=[function(e){return d[e]}];
          e=function(){return'\\w+'};c=1
        }; while(c--){
          if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}
        } return p
    }('$(\'.0-2 3, .0-2 7\').4(5(1){1.6()});',8,8,'dropdown|e|menu|input|click|function|stopPropagation|label'.split('|'),0,{}));

    /* document.getElementById("#country-search-input").tagsinput({
      typeahead: {
        source: ['Amsterdam', 'Washington', 'Sydney', 'Beijing', 'Cairo']
      }
    }); */

    $scope.searchFilterToggle = false;

    $scope.trailBtn = function (event){
      $(event.target).addClass('activeGreen');
    }

    $scope.flagBtn = function (event){
      $(event.target).addClass('activeRed');
    }

  }).
  controller('MyCtrl2', function ($scope) {
    // write Ctrl here

  });
