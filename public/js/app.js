var myApp = angular.module('myApp', [
  'ui.state',
  'wu.masonry',
  'myApp.controllers',
  'myApp.filters',
  'myApp.services',
  'myApp.directives'
]).
config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'partials/index.html',
      controller: 'IndexController'
  })
    .state('home.discover', {
      url: 'discover',
      templateUrl: 'partials/discover.html',
      controller: 'IndexController'
  });
  $urlRouterProvider.otherwise('/discover');
  $locationProvider.html5Mode(true);
})
.value('$anchorScroll', angular.noop);